<?php

namespace App\Controller\Admin;

use App\Service\LogDataFormatter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LogController extends AbstractController
{
    /** @var LogDataFormatter */
    private $logDataFormatter;

    /**
     * LogController constructor.
     *
     * @param LogDataFormatter $logDataFormatter
     */
    public function __construct(LogDataFormatter $logDataFormatter)
    {
        $this->logDataFormatter = $logDataFormatter;
    }

    /**
     * @Route("/admin/http-log", name="admin_http_logs")
     * @param Request $request
     *
     * @return Response
     */
    public function httpLog(Request $request)
    {
        $clientIp = $request->query->get('clientIp');
        $filename = __DIR__ . '/../../../var/log/http_request_response.log';

        if (false === file_exists($filename)) {
            return $this->render('error.html.twig', ['message' => 'Log file does not exist']);
        }

        $file   = new \SplFileObject($filename);
        $result = [];

        while ($file->eof() !== true) {
            $logData = json_decode($file->fgets(), true);

            if (empty($logData) || (null !== $clientIp && $logData['context']['clientIp'] !== $clientIp)) {
                continue;
            }

            $result[] = $this->logDataFormatter->formatLogData($logData);
        }

        return $this->render('log.html.twig', ['table' => $result]);
    }
}
