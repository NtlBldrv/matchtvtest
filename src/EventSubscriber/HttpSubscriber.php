<?php

namespace App\EventSubscriber;

use App\Service\Logger\HttpLogger;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class HttpSubscriber implements EventSubscriberInterface
{
    /** @var LoggerInterface */
    private $logger;

    /**
     * HttpSubscriber constructor.
     *
     * @param HttpLogger $logger
     */
    public function __construct(HttpLogger $logger)
    {
        $this->logger = $logger;
    }

    /** {@inheritDoc} */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => ['onRequest', 100]
        ];
    }

    /**
     * @param ResponseEvent $event
     *
     * @throws \Exception
     */
    public function onRequest(ResponseEvent $event)
    {
        if ($event->getRequest()->headers->get('log') === 'true') {
            $this->logger->info(
                'Http Request',
                [
                    'requestUrl' => $event->getRequest()->getUri(),
                    'request'    => [
                        'headers' => (string)$event->getRequest()->headers,
                        'body'    => $event->getRequest()->request->all(),
                    ],
                    'response'   => [
                        'headers' => (string)$event->getResponse()->headers,
                        'body'    => $event->getResponse()->getContent(),
                    ],
                    'httpCode'   => $event->getResponse()->getStatusCode(),
                    'clientIp'   => $event->getRequest()->getClientIp(),
                    'timestamp'  => (new \DateTime())->format('d.m.Y H:i:s'),
                ]
            );
        }
    }
}
