<?php

namespace App\Service;

class LogDataFormatter
{
    /**
     * @param array $logData
     *
     * @return array
     */
    public function formatLogData(array $logData): array
    {
        $requestHeaders = $logData['context']['request']['headers'];
        $requestBody = json_encode($logData['context']['request']['body']);
        $logData['context']['request'] = sprintf('headers: %s, body: %s', $requestHeaders, $requestBody);

        $responseHeaders = $logData['context']['response']['headers'];
        $logData['context']['response'] = sprintf('headers: %s, body: %s', $responseHeaders, $logData['context']['response']['body']);

        return $logData;
    }
}
